
import java.util.ArrayList;
import java.util.Arrays;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.GraphView;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.UndirectedGraph;
import org.openide.util.Lookup;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mob
 */
public class SubGraph
{
    
    public static UndirectedGraph GetNeighboring(Node startNode, UndirectedGraph graph)
    {
        // create subgraph
        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        GraphView newView = graphModel.newView();     //Duplicate main view
        UndirectedGraph subGraph = graphModel.getUndirectedGraph(newView);
        subGraph.clear();
        
        ArrayList<Node> neighbors = new ArrayList<>(Arrays.asList(graph.getNeighbors(startNode).toArray()));
        //Add Nodes
        for(Node node: neighbors)
        {
           subGraph.addNode(node);
        }
        
        // Then add edges
        for(Node node: neighbors)
        {
           for(Edge edge: graph.getEdges(node).toArray())
           {
               if(neighbors.contains(edge.getTarget()) && neighbors.contains(edge.getSource()))
               {
                   subGraph.addEdge(edge);
               }
           }      
        }
        
        return subGraph;
    }
     
}
