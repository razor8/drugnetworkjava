import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.HierarchicalGraph;
import org.gephi.graph.api.Node;
import org.gephi.statistics.plugin.ClusteringCoefficient;
import org.gephi.statistics.plugin.GraphDistance;
import org.gephi.statistics.plugin.PageRank;
import org.openide.util.Lookup;

public class GraphAnal {

    // returns page ranks for each node
    public static Map<Node, Double> getPageRanks(Graph graph)
    {
        Map<Node, Double> result = new HashMap<>();
        AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
        
        // execute gephi page rank 
        PageRank pagerank = new PageRank();
        pagerank.setProbability(0.15);
        pagerank.execute(graph.getGraphModel(), attributeModel);
        
        for (Node node : graph.getNodes())
        {
            result.put(node, (double) node.getAttributes().getValue(PageRank.PAGERANK));
        }
        return result;
    }

    //Gibt nach PageRank sortiert die ersten <length> Nodes in einer ArrayList zur�ck.
    public static List<Map.Entry> getPageRanksSorted(Graph graph, int length)
    {
        Map<Node, Double> pageRanks = getPageRanks(graph);
        List<Map.Entry> sortedPageRanks = new ArrayList<Map.Entry>(pageRanks.entrySet());
        
        Collections.sort(sortedPageRanks,
                new Comparator()
                {
                    public int compare(Object o1, Object o2)
                    {
                        Map.Entry e1 = (Map.Entry) o1;
                        Map.Entry e2 = (Map.Entry) o2;
                        return ((Comparable) e2.getValue()).compareTo(e1.getValue());
                    }
                });

        return sortedPageRanks.subList(0, length);
    }
    
    // returns clustering coefficient for given graph
    public static double getClusteringCoefficients(Graph graph)
    {
        AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();

        ClusteringCoefficient clustering = new ClusteringCoefficient();
        clustering.execute((HierarchicalGraph) graph, attributeModel);
        return clustering.getAverageClusteringCoefficient() / graph.getNodeCount();
    }
    
    // returns betweenness centralities for each node
    public static Map<Node, Double> getBetweennessCentralities(Graph graph)
    {
        Map<Node, Double> result = new HashMap<>();
        AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
        GraphDistance distance = new GraphDistance();
        distance.execute((HierarchicalGraph) graph, attributeModel);

        for (Node node : graph.getNodes().toArray())
        {
            result.put(node, (Double) node.getAttributes().getValue(GraphDistance.BETWEENNESS));
        }
        return result;
    }
}
