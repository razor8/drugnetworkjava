import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;

public class Parser {
	
	//public ArrayList<Node> nodes = new ArrayList<Node>();
	HashMap<Integer,Node> nodes = new HashMap<Integer,Node>();
	public ArrayList<Edge> edges = new ArrayList<Edge>();	
	
	public Parser(Path path, GraphModel graphModel){
		Charset charset = Charset.forName("US-ASCII");
		try (BufferedReader reader = Files.newBufferedReader(path, charset)) {
			String line = null;
			String[] array;

			//int c = 0;
                        System.out.println("Adding Nodes ");
			ArrayList<String[]> data = new ArrayList<String[]>();
			while ((line = reader.readLine()) != null){
				array = line.split(" ");
				if(array.length < 1)continue;
				data.add(array);
				Node first = graphModel.factory().newNode(array[0]);
				first.getNodeData().setLabel(array[0]);
				nodes.put(Integer.parseInt(array[0]),first);
				//System.out.println("Adding Node " + array[0]);
			}
			//int nodeCount = nodes.size();
                        System.out.print("Adding Edges: ");
			for(int i = 0; i<data.size();++i){
				array = data.get(i);
				Integer first = Integer.parseInt(array[0]);
				if(nodes.containsKey(first) == false) continue;
				Node f = nodes.get(first);
				
				for(int j = 1;j<array.length;++j){
					
					Integer index = Integer.parseInt(array[j]);
					
					if(nodes.containsKey(index)){	
						//System.out.println("Adding Edge " + first +"->" + index);
						
						Node n = nodes.get(index);

						edges.add(graphModel.factory().newEdge(f,n));
					}
				}
                                if(i % (data.size() / 10) == 0 && i != 0)
                                    System.out.print( (int) (((float) i / data.size()) * 100)  +"% ");
			}
                        System.out.print("100% \n");
                        
			data.clear();
		}catch (Exception e){
			System.err.println(e);
		}
	}

}
