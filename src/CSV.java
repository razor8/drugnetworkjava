
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Stack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mob
 */
public class CSV<T>
{
    //private T t;
    private String csv;
    private String header;
    public CSV()
    {
        this.header = "";
        this.csv = "";
    }
    
    public void AddStack(String name, Stack<T> stack)
    {
        this.csv += "\n\"" + name + "\"";
        for(T t: stack)
        {
            this.csv += ";" + t.toString();
        }
    }
    
    public void WriteCsv(String path) throws IOException
    {
        File file = new File(path);
        BufferedWriter outputWriter = new BufferedWriter(new FileWriter(file));
        outputWriter.write(csv);
        outputWriter.close();
    }
}
