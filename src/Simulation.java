import java.io.IOException;
import java.util.Random;
import java.util.Stack;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.NodeIterable;
import org.gephi.graph.api.UndirectedGraph;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mob
 */
public class Simulation
{
    double money;
    double c_k;
    double c_t;
    double profit;
   
    int iterationCounter = 10; // r1 = 10, r2 = 5
    Stack<Node> visited = new Stack<>();
    int userCount = 0;
    boolean isBroke = false;
    UndirectedGraph graph;
    int amountOfSubGraphs;
    Random rand = new Random();
    Stack<Node> neighborIterator = new Stack<>();
    Stack<Integer> moneyStack = new Stack<>();
    Stack<Double> probabilityStack = new Stack<>();
    Stack<Integer> neighboringStack = new Stack<>();
    Stack<Integer> degreeStack = new Stack<>();
    
    public Simulation(double money, double c_k, double c_t, double profit, UndirectedGraph graph)
    {
        this.money = money;
        this.c_k = c_k;
        this.c_t = c_t;
        this.profit = profit;
        this.graph = graph;
        
    }
    
    public void StartSimulation()
    {
        ViralStrategy viral = new ViralStrategy(graph);
        viral.Init();
        viral.GetAttributes();
        amountOfSubGraphs = viral.GetAmountOfSubGraphs();
        Stack<Node> startNodes = new Stack<>();
        
        --amountOfSubGraphs;
        startNodes.addAll(viral.GetInfectionCandidates());
        
        for(Node node: startNodes)
        {
            Invest(node);
            NodeIterable neighbors = graph.getNeighbors(node);
            IterationStep(neighbors);
        }
        
        while(!isBroke && iterationCounter > 0)
        {
            moneyStack.push((int) Math.ceil(money));
            --iterationCounter;
            //System.out.println("money: " + money);
            Stack<Node> currentIterator = neighborIterator;
            neighborIterator.empty();
            while(currentIterator.isEmpty() == false)
            {
                IterationStep(graph.getNeighbors(currentIterator.pop()));
            }
            if(amountOfSubGraphs > 0)
            {
                --amountOfSubGraphs;
                for(Node node: viral.GetInfectionCandidates())
                {
                    Invest(node);
                    NodeIterable neighbors = graph.getNeighbors(node);
                    IterationStep(neighbors);
                    
                }
            }
            else
            {
                viral.GetAttributes();
                amountOfSubGraphs = viral.GetAmountOfSubGraphs();
            }
            
            
        }

    }
    
    private void Invest(Node node)
    {
        if(node.getNodeData().getLabel().equals("user") == false)
        {
            double costs =  graph.getDegree(node) * c_k;
            if(money < costs)
            {
                
                //moneyStack.push();
                //System.out.println("money: " + money);
                return;
            }
            money -= costs;
            node.getNodeData().setLabel("user");
        }
        
    }
    
    
    private void IterationStep(NodeIterable neighbors)
    {
        for(Node node: neighbors)
        {
            String nodeLabel = node.getNodeData().getLabel();
            if(nodeLabel.equals("visited")) // prevent visiting the same node 2 times
            {
                //continue;
            }
            if(nodeLabel.equals("user") == false)
            {
                if(rand.nextBoolean() && (money > c_t))
                {
                    node.getNodeData().setLabel("visited");
                    neighborIterator.push(node);
                    visited.push(node);
                    money -= c_t;
                    
                    int neighborUserCount = 0;
                    for(Node neighbor: graph.getNeighbors(node))
                    {
                        if(neighbor.getNodeData().getLabel().equals("user") == true)
                        {
                            ++neighborUserCount;
                        }
                    }
                    double nextRand = rand.nextDouble();
                    double currentNodeDegree = (double) graph.getDegree(node);
                    
                    probabilityStack.push(((double) neighborUserCount / currentNodeDegree));
                    neighboringStack.push(neighborUserCount);
                    degreeStack.push((int)currentNodeDegree);
                    
                    if(nextRand < ((double) neighborUserCount / currentNodeDegree))
                    {
                        
                        
                        /*System.out.println("rand: " + nextRand + 
                                "   \tprobability: " + ((double) neighborUserCount / (double) graph.getDegree(node)) +  
                                "   \tneighboringUsers: " + neighborUserCount);*/
                        money += profit;
                        ++userCount;
                        node.getNodeData().setLabel("user");
                    }
                }else if(money < c_t)
                {
                    isBroke = true;
                }
                
            }
        } 
    }
    
    public void PrintStatistics()
    {
        CSV csv = new CSV();
        csv.AddStack("Money", moneyStack);
        csv.AddStack("Probability", probabilityStack);
        csv.AddStack("Neighbors", neighboringStack);
        csv.AddStack("Dergrees", degreeStack);
        try
        {
            csv.WriteCsv("statistics_caviar.csv");
        }
        catch(IOException e)
        {
            System.err.println("Unable to write .csv: " + e.getMessage());
        }
        
        System.out.println("Money: ");
        for(Integer d: moneyStack)
        {
            System.out.print(d + " ");
        }
        
        
        
        System.out.println("\nVisited: " + visited.size());
        System.out.println("Users: " + userCount);
    }
}
