
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.UndirectedGraph;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mob
 */
public class ViralStrategy
{
    UndirectedGraph graph;
    List<Map.Entry> pageRanks;
    Map<UndirectedGraph,Double> clusteringCoefficients;
    Map<UndirectedGraph,Map<Node,Double>> betweennessCentralities;
    Map<Node,UndirectedGraph> subgraphs;
    
    // parameters for graph metrics
    int amountOfSubGraphs = 3;
    int numberOfInfectionCandidates = 100;
    Double betweennessTreshold = 0.0001;
    
    
    public ViralStrategy(UndirectedGraph graph)
    {
        this.graph = graph;
        this.clusteringCoefficients = new HashMap<>();
        this.betweennessCentralities = new HashMap<>();
        this.subgraphs = new HashMap<>();
    }
    
    // calculate page rank once
    public void Init()
    {
        // get highest pageRanks
        System.out.println("Calculating page ranks");
        pageRanks = GraphAnal.getPageRanksSorted(graph, amountOfSubGraphs);
    }
    
    // if needed recalculate graph attributes with updated graph
    public void GetAttributes()
    {
        betweennessCentralities.clear();
        subgraphs.clear();
        
        
        // build subgraphs from highest pageRanks
        
        System.out.println("Building subgraphs");
        int subGraphCounter = 0;
        for(Map.Entry<Node,Double> entry: pageRanks)
        {
            System.out.print((int) (((double) subGraphCounter / (double) amountOfSubGraphs) * 100) + "% ");
            subgraphs.put(entry.getKey(), SubGraph.GetNeighboring(entry.getKey(), graph));
            ++subGraphCounter;
        }
        System.out.println("100%");
        // get clusteringCoefficients of subgraphs
        System.out.println("Calculating clustering coefficients");
        for(Map.Entry<Node,UndirectedGraph> entry: subgraphs.entrySet())
        {
            clusteringCoefficients.put(entry.getValue(), GraphAnal.getClusteringCoefficients(entry.getValue()));
            //System.out.println( entry.getKey().getId()+ ":\t" + clusteringCoefficients.get(entry.getValue()));
        }
        
        // get betweennesCenttralities of subgraphs
        System.out.println("Calculating betweenness centralities");
        for(Map.Entry<Node,UndirectedGraph> entry: subgraphs.entrySet())
        {
            //System.out.println(entry.getKey().getId()+ " Degree:\t" +graph.getDegree(entry.getKey()));
            betweennessCentralities.put(entry.getValue(), GraphAnal.getBetweennessCentralities(entry.getValue()));
        }
    }
    
    // max GetInfectionCandidates calls before GetAttributes needs to be called again
    public int GetAmountOfSubGraphs()
    {
        return amountOfSubGraphs;
    }
    
    // returns list of start node candidates
    public ArrayList<Node> GetInfectionCandidates()
    {       
        ArrayList<Node> infectionCandidates = new ArrayList<>();
        UndirectedGraph clusterGraph = GetMaxClustered();
        for(int i = 0; i < numberOfInfectionCandidates; ++i)
        {
            Node candidate = GetInfectionCandidate(clusterGraph);
            if(candidate != null)
            {
                infectionCandidates.add(candidate);
            }
        }
        
        return infectionCandidates;
    }
    
    //returns "best" start node candidate
    private Node GetInfectionCandidate(UndirectedGraph currentGraph)
    {
        Node candidate = null;
        
        // get best InfectionCandidate depending on his betweenness Centrality
        Double maxBetweenness = 0.0;
        for(Map.Entry<Node, Double> entry: betweennessCentralities.get(currentGraph).entrySet())
        {
            if(entry.getValue() > maxBetweenness)
            {
                candidate = entry.getKey();
                maxBetweenness = entry.getValue();
            }
        }
        betweennessCentralities.get(currentGraph).remove(candidate);
        // betweennessTreshold can be used to clip candidates with lower betweenness centrality
        if(maxBetweenness < betweennessTreshold)
        {
            return null;
        }
        
        return candidate;
    }
    
    // returns graph with highest clustering coefficient
    private UndirectedGraph GetMaxClustered()
    {
        UndirectedGraph result = null;
        
        Double maxClusteringCoefficient = 0.0;
        for(Map.Entry<UndirectedGraph,Double> entry: clusteringCoefficients.entrySet())
        {
            if(entry.getValue() > maxClusteringCoefficient)
            {
                result = entry.getKey();
                maxClusteringCoefficient = entry.getValue();
            }
        }
        
        // after use, remove graph from list
        clusteringCoefficients.remove(result);
        return result;
    }

    
}
