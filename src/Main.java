import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.gephi.graph.api.*;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.project.api.*;
import org.openide.util.Lookup;

public class Main {

	public static void main(String[] args) {
		// Init a project and workspace
		ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
		pc.newProject();
		Workspace workspace = pc.getCurrentWorkspace();

		// Get graph model
		GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();

		// Create nodes
		//Node n0 = graphModel.factory().newNode("n0");
		//n0.getNodeData().setLabel("Node 0");
		//Node n1 = graphModel.factory().newNode("n1");
		//n1.getNodeData().setLabel("Node 1");
		
		//Parse File
		Path path = FileSystems.getDefault().getPath(".", "caviar.txt");//symmetric_friends caviar
		
		Parser parsi = new Parser(path, graphModel);

		//Draw Graph
		
                UndirectedGraph undirectedGraph = graphModel.getUndirectedGraph();
		for (Node n : parsi.nodes.values()){			
			undirectedGraph.addNode(n);
		}
		for (Edge e : parsi.edges){
			undirectedGraph.addEdge(e);
		}
		parsi.edges.clear();
		parsi.nodes.clear();
		System.out.println("Nodes: " + undirectedGraph.getNodeCount() +  "\tEdges: " + undirectedGraph.getEdgeCount());
		
                
                
                Simulation sim = new Simulation(30000.0, 4.0, 0.05, 100.0, undirectedGraph);    // r1 = (30000.0, 4.0, 0.05, 100) mit 10 Iterationsschritten
                //Simulation sim = new Simulation(30000.0, 0.1, 0.05, 100.0, undirectedGraph);    // r2 = (30000.0, 0.1, 0.05, 100) mit 5 Iterationsschritten
                sim.StartSimulation();
                sim.PrintStatistics();
		// Export File
		/*ExportController ec = Lookup.getDefault().lookup(ExportController.class);
		try {
			ec.exportFile(new File("test.gexf"));
		} catch (IOException ex) {
			ex.printStackTrace();
			return;
		}*/
	}

}
